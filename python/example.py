import time
import json
import httplib2
import socket

from settings import *
from queue import Queue
from threading import Thread
from serps.client import Client
from serps.authentication import Authentication


def read_keywords_csv_file(file_path):
    """
    Read keywords from csv file
    :param file_path: the file path
    :return:
    """
    rows = list()

    with open(file_path, 'r') as file:
        rows = file.readlines()

    file.close()
    return rows


def get_default_query():
    """

    :return:
    """
    return {
        'search_engine': 'google',
        'region': None,
        'language': None,
        'strategy': 'standard',
        'max_results': 10,
        'search_type': 'web',
        'phrase': None,
        'user_agent': 'pc'
    }


def serps_worker(serps_client, queue):

    while not queue.empty():

        job_id = queue.get()

        no_try = 0

        while no_try < NO_RETRY:

            try:
                old_timeout = socket.getdefaulttimeout()

                socket.setdefaulttimeout(1)
                json_get_response = serps_client.get_serps(job_id)['content']
                socket.setdefaulttimeout(old_timeout)
                get_response = json.loads(json_get_response)

            except Exception as error:
                time.sleep(SLEEP_PER_TRY)
                no_try += 1
                continue


            if not get_response['ready']:
                print(job_id + " - Sleeping %d s"  % SLEEP_PER_TRY)
                time.sleep(SLEEP_PER_TRY)
                no_try += 1
                continue

            break

        print(get_response)
        print("###########################################")
        queue.task_done()



def main():
    """

    :return:
    """
    region = 'fr'
    language = 'fr'
    max_results = 10
    language, max_results
    file_path = 'keywords.csv'

    auth = Authentication(API_KEY, API_SECRET, API_SALT)
    transport = httplib2.Http()
    serps_client = Client(API_URL, auth, transport)

    file_rows = read_keywords_csv_file(file_path)
    queue = Queue()

    for row in file_rows:
        keyword = row.strip()

        query = get_default_query()
        query['language'] = language
        query['region'] = region
        query['max_results'] = max_results
        query['phrase'] = keyword

        json_post_response = serps_client.post_serps(query)
        post_response = json.loads(json_post_response['content'])
        job_id = post_response['jid']
        print("Creating Job-Id %s" % job_id)

        queue.put(job_id)

    print("###########################################")
    print()

    for thread_no in range(NO_THREADS):
         thread = Thread(target=serps_worker, args=(serps_client, queue))
         thread.daemon = True
         thread.start()

    queue.join()

if __name__ == '__main__':
    main()




