import hmac
import time
import hashlib


class Authentication(object):

    def __init__(self, api_key, api_secret, salt):
        """

        :return:
        """
        self.api_key = api_key
        self.api_secret = api_secret
        self.salt = salt

    @staticmethod
    def get_timestamp():
        """
        Get current UNIX timestamp
        :return:
        """
        return round(time.time())

    def compute_hash(self):
        """
        Create authentication header
        :return:
        """
        current_time = self.get_timestamp()

        hash_source = '{timestamp}{api_key}{salt}'.format(
            timestamp=current_time,
            api_key=self.api_key,
            api_secret=self.api_secret,
            salt=self.salt
        )

        hash_value = hmac.new(
            bytes(self.api_secret, encoding='UTF-8'),
            bytes(hash_source,  encoding='UTF-8'),
            hashlib.sha256
        ).hexdigest()

        header = {
            'Authorization': 'KeyAuth publicKey={pb_key} hash={hash} ts={ts}'.format(
                pb_key=self.api_key,
                hash=hash_value,
                ts=current_time
            )
        }

        return header

