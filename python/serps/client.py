import json


class Client(object):

    def __init__(self, api_url, authentication, transport):
        """

        :param api_url:
        :param authentication:
        :param transport:
        :return:
        """
        self.api_url = api_url
        self.authentication = authentication
        self.transport = transport

    def post_serps(self, data):
        """
        Asynchronous SERPs processing request
        Creates a new job to be processed. Upon successfully accepting the request you will
        receive a job id that can later be used to retrieve the SERPs data (see GET method below ).
        :param data: API request
        :return:
        """
        auth_header = self.authentication.compute_hash()
        auth_header['Content-Type'] = 'application/json'

        url = '{api_url}/serps/'.format(api_url=self.api_url)

        response, content = self.transport.request(
            uri=url,
            method='POST',
            headers=auth_header,
            body=json.dumps(data)
        )

        return {'header': response, 'content': content.decode('utf-8')}

    def get_serps(self, job_id):
        """
        Returns information about an existing job.
        After successfully requesting a job (see POST method on how to request a job),
        you can use this request to get available information for the returned job id
        :param job_id: the job id
        :return:
        """
        auth_header = self.authentication.compute_hash()

        url = '{api_url}/serps/{job_id}'.format(
            api_url=self.api_url,
            job_id=job_id
        )

        response, content = self.transport.request(
            uri=url,
            method='GET',
            headers=auth_header
        )

        return {
            'header': response,
            'content': content.decode('utf-8')
        }
