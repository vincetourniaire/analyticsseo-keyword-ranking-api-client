# analyticsseo-keyword-ranking-api-client
## Python3 CLient Example

### Configuration

```
cp settings.py-dist settings.py
```

Then enter you API key, your API secret and API salt

### Installation
```
sudo python3 setup.py install
```
 
### Run
``` 
python3 run.py
```