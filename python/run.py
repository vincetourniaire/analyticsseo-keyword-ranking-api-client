import time
import json
import httplib2

from settings import *
from serps.client import Client
from serps.authentication import Authentication

auth = Authentication(API_KEY, API_SECRET, API_SALT)
transport = httplib2.Http()
serps_client = Client(API_URL, auth, transport)

serps_query = {
    'search_engine': 'google',
    'region': 'fr',
    'language': 'fr',
    'strategy': 'standard',
    'max_results': 10,
    'search_type': 'web',
    'phrase': 'rugby toulon',
    'user_agent': 'pc'
}

json_post_response = serps_client.post_serps(serps_query)
post_response = json.loads(json_post_response['content'])
job_id = post_response['jid']

print('##################################################')
print('job_id: %s' % job_id)
print('##################################################')

no_try = 0

while no_try < NO_RETRY:

    json_get_response = serps_client.get_serps(job_id)['content']
    get_response = json.loads(json_get_response)

    if not get_response['ready']:
        print('Result not ready, sleeping %d s ...' % SLEEP_PER_TRY)
        time.sleep(SLEEP_PER_TRY)
        no_try += 1
        continue

    break

print('##################################################\n')
print(get_response)