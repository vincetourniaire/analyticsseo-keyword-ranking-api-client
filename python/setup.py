from setuptools import setup, find_packages

setup(
    name='analyticsseo-keyword-ranking-api-client',
    version='1.0',
    packages=find_packages(),
    url='http://docs.analyticsseo.com/serps/',
    author='Vincent Tourniaire',
    author_email='vincent.tourniaire@analyticsseo.com',
    requires=['httplib2==0.8'],
    description='Analytics SEO Structured Keyword Ranking Api API Client Example'
)
